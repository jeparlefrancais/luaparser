//! A parser that reads Lua 5.1 code and produces an abstract syntax tree (AST).
mod parser;
mod fast_parser;

#[cfg(any(test, feature = "nodes"))]
pub mod nodes;
#[cfg(any(test, feature = "nodes"))]
pub mod parser_impl;

pub use parser::{builders, NodeTypes, ParsingError};
pub use fast_parser::FastParser;
