//! A concrete parser implemented from the generic using the nodes within the `luaparser::nodes`
//! module.

use crate::nodes::*;

use crate::FastParser;
use crate::parser::NodeTypes;

pub struct FastParserNodes;

impl NodeTypes for FastParserNodes {
    type Block = Block;
    type Statement = Statement;
    type LastStatement = LastStatement;

    type AssignStatement = AssignStatement;
    type DoStatement = DoStatement;
    type CallStatement = FunctionCall;
    type FunctionStatement = FunctionStatement;
    type GenericForStatement = GenericForStatement;
    type IfStatement = IfStatement;
    type LocalAssignStatement = LocalAssignStatement;
    type LocalFunctionStatement = LocalFunctionStatement;
    type NumericForStatement = NumericForStatement;
    type RepeatStatement = RepeatStatement;
    type WhileStatement = WhileStatement;

    type Expression = Expression;

    type Arguments = Arguments;
    type Prefix = Prefix;

    type Variable = Variable;
    type FieldExpression = FieldExpression;
    type IndexExpression = IndexExpression;

    type BinaryOperator = BinaryOperator;
    type BinaryExpression = BinaryExpression;
    type CallExpression = FunctionCall;
    type FunctionExpression = FunctionExpression;
    type NumberExpression = NumberExpression;
    type StringExpression = StringExpression;
    type TableEntry = TableEntry;
    type TableExpression = TableExpression;
    type UnaryOperator = UnaryOperator;
    type UnaryExpression = UnaryExpression;
}

#[derive(Default)]
pub struct FastParserImpl;

impl FastParser for FastParserImpl {
    type Types = FastParserNodes;
}
