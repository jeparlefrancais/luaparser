use crate::parser::{
    parse_chunk,
    ParsingError,
};
use crate::parser::NodeTypes;

use lualexer::{FastLexer, Token, TokenType};

/// A trait that creates a parser from the given NodeTypes implementation.
pub trait FastParser {
    type Types: NodeTypes;

    fn parse<'a>(&self, input: &'a str) -> Result<<Self::Types as NodeTypes>::Block, ParsingError> {
        let tokens: Vec<Token<'a>> = FastLexer::new()
            .parse(input)
            .map_err(|lexer_error| ParsingError::Lexer(lexer_error.0))?
            .drain(..)
            .filter(|token| !token.is_type(TokenType::Comment))
            .collect();

        parse_chunk::<Self::Types>(&tokens)
    }
}
