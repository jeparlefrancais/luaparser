//! A collection of nodes that can be used to collect the abstract syntax tree data produced
//! by the parser. These nodes are used by the parser within the `parser_impl` module.

mod expressions;
mod function_call;
mod statements;

pub use expressions::*;
pub use function_call::*;
pub use statements::*;

#[derive(Clone, Debug, PartialEq, Eq, Default)]
pub struct Block {
    pub statements: Vec<Statement>,
    pub last_statement: Option<LastStatement>,
}

impl From<(Vec<Statement>, Option<LastStatement>)> for Block {
    fn from((statements, last_statement): (Vec<Statement>, Option<LastStatement>)) -> Self {
        Self {
            statements,
            last_statement,
        }
    }
}
