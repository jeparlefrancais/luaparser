use crate::nodes::{
    Expression,
    Statement,
};

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct LocalAssignStatement {
    pub variables: Vec<String>,
    pub values: Vec<Expression>,
}

impl From<(Vec<String>, Vec<Expression>)> for LocalAssignStatement {
    fn from((variables, values): (Vec<String>, Vec<Expression>)) -> Self {
        Self {
            variables,
            values,
        }
    }
}

impl Into<Statement> for LocalAssignStatement {
    fn into(self) -> Statement {
        Statement::LocalAssign(self)
    }
}
