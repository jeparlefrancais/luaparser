use crate::nodes::{
    Block,
    Expression,
    Statement,
};

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct RepeatStatement {
    pub condition: Expression,
    pub block: Block,
}

impl From<(Expression, Block)> for RepeatStatement {
    fn from((condition, block): (Expression, Block)) -> Self {
        Self { condition, block }
    }
}

impl Into<Statement> for RepeatStatement {
    fn into(self) -> Statement {
        Statement::Repeat(self)
    }
}
