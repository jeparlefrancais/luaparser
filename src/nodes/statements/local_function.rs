use crate::nodes::{
    Block,
    Statement,
};

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct LocalFunctionStatement {
    pub identifier: String,
    pub block: Block,
    pub parameters: Vec<String>,
    pub is_variadic: bool,
}

impl From<(String, Vec<String>, bool, Block)> for LocalFunctionStatement {
    fn from((identifier, parameters, is_variadic, block): (String, Vec<String>, bool, Block)) -> Self {
        Self {
            identifier,
            block,
            parameters,
            is_variadic,
        }
    }
}

impl Into<Statement> for LocalFunctionStatement {
    fn into(self) -> Statement {
        Statement::LocalFunction(self)
    }
}
