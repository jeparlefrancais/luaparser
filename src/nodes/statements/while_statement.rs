use crate::nodes::{
    Block,
    Expression,
    Statement,
};

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct WhileStatement {
    pub condition: Expression,
    pub block: Block,
}

impl From<(Expression, Block)> for WhileStatement {
    fn from((condition, block): (Expression, Block)) -> Self {
        Self { condition, block }
    }
}

impl Into<Statement> for WhileStatement {
    fn into(self) -> Statement {
        Statement::While(self)
    }
}
