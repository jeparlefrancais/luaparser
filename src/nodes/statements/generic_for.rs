use crate::nodes::{
    Block,
    Expression,
    Statement,
};

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct GenericForStatement {
    pub identifiers: Vec<String>,
    pub expressions: Vec<Expression>,
    pub block: Block,
}

impl From<(Vec<String>, Vec<Expression>, Block)> for GenericForStatement {
    fn from((identifiers, expressions, block): (Vec<String>, Vec<Expression>, Block)) -> Self {
        Self {
            identifiers,
            expressions,
            block,
        }
    }
}

impl Into<Statement> for GenericForStatement {
    fn into(self) -> Statement {
        Statement::GenericFor(self)
    }
}
