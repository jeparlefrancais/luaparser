use crate::nodes::{
    Block,
    Statement,
};

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct FunctionStatement {
    pub name: String,
    pub field_names: Vec<String>,
    pub method: Option<String>,
    pub block: Block,
    pub parameters: Vec<String>,
    pub is_variadic: bool,
}

impl From<(String, Vec<String>, Option<String>, Block, Vec<String>, bool)> for FunctionStatement {
    fn from((name, field_names, method, block, parameters, is_variadic): (String, Vec<String>, Option<String>, Block, Vec<String>, bool)) -> Self {
        Self {
            name,
            field_names,
            method,
            block,
            parameters,
            is_variadic,
        }
    }
}

impl Into<Statement> for FunctionStatement {
    fn into(self) -> Statement {
        Statement::Function(self)
    }
}
