mod assign;
mod do_statement;
mod function_statement;
mod generic_for;
mod if_statement;
mod local_assign;
mod local_function;
mod numeric_for;
mod repeat_statement;
mod while_statement;

pub use assign::*;
pub use do_statement::*;
pub use function_statement::*;
pub use generic_for::*;
pub use if_statement::*;
pub use local_assign::*;
pub use local_function::*;
pub use numeric_for::*;
pub use repeat_statement::*;
pub use while_statement::*;

use crate::nodes::{Expression, FunctionCall};
use crate::parser::builders;

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Statement {
    Assign(AssignStatement),
    Do(DoStatement),
    Call(FunctionCall),
    Function(FunctionStatement),
    GenericFor(GenericForStatement),
    If(IfStatement),
    LocalAssign(LocalAssignStatement),
    LocalFunction(LocalFunctionStatement),
    NumericFor(NumericForStatement),
    Repeat(RepeatStatement),
    While(WhileStatement),
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum LastStatement {
    Break,
    Return(Vec<Expression>),
}

impl builders::LastStatement<Expression> for LastStatement {
    fn break_statement() -> Self {
        Self::Break
    }

    fn return_statement(expressions: Vec<Expression>) -> Self {
        Self::Return(expressions)
    }
}
