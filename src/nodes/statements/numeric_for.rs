use crate::nodes::{
    Block,
    Expression,
    Statement,
};

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct NumericForStatement {
    pub identifier: String,
    pub start: Expression,
    pub end: Expression,
    pub step: Option<Expression>,
    pub block: Block,
}

impl From<(String, Expression, Expression, Option<Expression>, Block)> for NumericForStatement {
    fn from((identifier, start, end, step, block): (String, Expression, Expression, Option<Expression>, Block)) -> Self {
        Self {
            identifier,
            start,
            end,
            step,
            block,
        }
    }
}

impl Into<Statement> for NumericForStatement {
    fn into(self) -> Statement {
        Statement::NumericFor(self)
    }
}
