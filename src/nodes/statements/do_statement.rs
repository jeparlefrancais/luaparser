use crate::nodes::{
    Block,
    Statement,
};

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct DoStatement {
    pub block: Block,
}

impl From<Block> for DoStatement {
    fn from(block: Block) -> Self {
        Self { block }
    }
}

impl Into<Statement> for DoStatement {
    fn into(self) -> Statement {
        Statement::Do(self)
    }
}
