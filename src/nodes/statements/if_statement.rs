use crate::nodes::{
    Block,
    Expression,
    Statement,
};

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct IfBranch {
    pub condition: Expression,
    pub block: Block,
}

impl From<(Expression, Block)> for IfBranch {
    fn from((condition, block): (Expression, Block)) -> Self {
        Self {
            condition,
            block,
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct IfStatement {
    pub branches: Vec<IfBranch>,
    pub else_block: Option<Block>,
}

impl From<(Vec<(Expression, Block)>, Option<Block>)> for IfStatement {
    fn from((branches, else_block): (Vec<(Expression, Block)>, Option<Block>)) -> Self {
        Self {
            branches: branches.into_iter().map(IfBranch::from).collect(),
            else_block,
        }
    }
}

impl Into<Statement> for IfStatement {
    fn into(self) -> Statement {
        Statement::If(self)
    }
}
