use crate::nodes::{
    Expression,
    FieldExpression,
    IndexExpression,
    Prefix,
    Statement,
};
use crate::parser::builders;

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Variable {
    Identifier(String),
    Field(Box<FieldExpression>),
    Index(Box<IndexExpression>),
}

impl builders::Variable<Prefix> for Variable {
    fn try_from_prefix(prefix: Prefix) -> Result<Self, Prefix> {
        match prefix {
            Prefix::Identifier(identifier) => Ok(Self::Identifier(identifier)),
            Prefix::Field(field) => Ok(Self::Field(field)),
            Prefix::Index(index) => Ok(Self::Index(index)),
            _ => Err(prefix)
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct AssignStatement {
    pub variables: Vec<Variable>,
    pub values: Vec<Expression>,
}

impl From<(Vec<Variable>, Vec<Expression>)> for AssignStatement {
    fn from((variables, values): (Vec<Variable>, Vec<Expression>)) -> Self {
        Self {
            variables,
            values,
        }
    }
}

impl Into<Statement> for AssignStatement {
    fn into(self) -> Statement {
        Statement::Assign(self)
    }
}
