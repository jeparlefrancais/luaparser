use crate::nodes::{
    Expression,
    Prefix,
    Statement,
    TableExpression,
    StringExpression,
};
use crate::parser::builders;

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct FunctionCall {
    pub prefix: Box<Prefix>,
    pub arguments: Arguments,
    pub method: Option<String>,
}

impl From<(Prefix, Arguments, Option<String>)> for FunctionCall {
    fn from((prefix, arguments, method): (Prefix, Arguments, Option<String>)) -> Self {
        Self {
            prefix: Box::new(prefix),
            arguments,
            method
        }
    }
}

impl Into<Expression> for FunctionCall {
    fn into(self) -> Expression {
        Expression::Call(Box::new(self))
    }
}

impl Into<Statement> for FunctionCall {
    fn into(self) -> Statement {
        Statement::Call(self)
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Arguments {
    Tuple(Vec<Expression>),
    String(StringExpression),
    Table(TableExpression),
}

impl builders::Arguments<Expression, TableExpression> for Arguments {
    fn from_string(string: String) -> Self {
        Self::String(StringExpression::from(string))
    }
    fn from_table(table: TableExpression) -> Self { Self::Table(table) }

    fn from_expressions(expressions: Vec<Expression>) -> Self {
        Self::Tuple(expressions)
    }
}
