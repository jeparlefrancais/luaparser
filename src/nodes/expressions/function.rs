use crate::nodes::{
    Block,
    Expression,
};

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct FunctionExpression {
    pub block: Block,
    pub parameters: Vec<String>,
    pub is_variadic: bool,
}

impl From<(Vec<String>, bool, Block)> for FunctionExpression {
    fn from((parameters, is_variadic, block): (Vec<String>, bool, Block)) -> Self {
        Self {
            block,
            parameters,
            is_variadic,
        }
    }
}

impl Into<Expression> for FunctionExpression {
    fn into(self) -> Expression {
        Expression::Function(self)
    }
}
