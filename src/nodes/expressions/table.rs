use crate::nodes::Expression;
use crate::parser::builders;

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum TableEntry {
    Field(String, Expression),
    Index(Expression, Expression),
    Value(Expression),
}

impl builders::TableEntry<Expression> for TableEntry {
    fn from_value(value: Expression) -> Self { Self::Value(value) }
    fn from_field(field: String, value: Expression) -> Self { Self::Field(field, value) }
    fn from_index(key: Expression, value: Expression) -> Self { Self::Index(key, value) }
}

impl From<Expression> for TableEntry {
    fn from(value: Expression) -> Self {
        Self::Value(value)
    }
}

impl From<(String, Expression)> for TableEntry {
    fn from((field, value): (String, Expression)) -> Self {
        Self::Field(field, value)
    }
}

impl From<(Expression, Expression)> for TableEntry {
    fn from((key, value): (Expression, Expression)) -> Self {
        Self::Index(key, value)
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct TableExpression {
    entries: Vec<TableEntry>,
}

impl From<Vec<TableEntry>> for TableExpression {
    fn from(entries: Vec<TableEntry>) -> Self {
        Self { entries }
    }
}

impl Into<Expression> for TableExpression {
    fn into(self) -> Expression {
        Expression::Table(self)
    }
}
