use crate::nodes::Expression;
use crate::parser::builders;

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct BinaryExpression {
    pub left: Expression,
    pub right: Expression,
    pub operator: BinaryOperator
}

impl Into<Expression> for BinaryExpression {
    fn into(self) -> Expression {
        Expression::Binary(Box::new(self))
    }
}

impl From<(Expression, BinaryOperator, Expression)> for BinaryExpression {
    fn from((left, operator, right): (Expression, BinaryOperator, Expression)) -> Self {
        Self {
            left,
            operator,
            right,
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum BinaryOperator {
    And,
    Or,
    Equal,
    NotEqual,
    LowerThan,
    LowerOrEqualThan,
    GreatherThan,
    GreatherOrEqualThan,
    Plus,
    Minus,
    Asterisk,
    Slash,
    Percent,
    Caret,
    Concat,
}

impl builders::BinaryOperator for BinaryOperator {
    fn and() -> Self { Self::And }
    fn or() -> Self { Self::Or }
    fn equal() -> Self { Self::Equal }
    fn not_equal() -> Self { Self::NotEqual }
    fn lower_than() -> Self { Self::LowerThan }
    fn lower_or_equal_than() -> Self { Self::LowerOrEqualThan }
    fn greather_than() -> Self { Self::GreatherThan }
    fn greather_or_equal_than() -> Self { Self::GreatherOrEqualThan }
    fn plus() -> Self { Self::Plus }
    fn minus() -> Self { Self::Minus }
    fn asterisk() -> Self { Self::Asterisk }
    fn slash() -> Self { Self::Slash }
    fn percent() -> Self { Self::Percent }
    fn caret() -> Self { Self::Caret }
    fn concat() -> Self { Self::Concat }
}
