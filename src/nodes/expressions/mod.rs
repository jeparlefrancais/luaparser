mod binary;
mod field;
mod function;
mod index;
mod number;
mod prefix;
mod string;
mod table;
mod unary;

pub use binary::*;
pub use field::*;
pub use function::*;
pub use index::*;
pub use number::*;
pub use prefix::*;
pub use string::*;
pub use table::*;
pub use unary::*;

use crate::nodes::{
    FunctionCall,
};
use crate::parser::builders;

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Expression {
    Binary(Box<BinaryExpression>),
    Call(Box<FunctionCall>),
    False,
    Field(Box<FieldExpression>),
    Function(FunctionExpression),
    Identifier(String),
    Index(Box<IndexExpression>),
    Nil,
    Number(NumberExpression),
    Parenthese(Box<Expression>),
    String(StringExpression),
    Table(TableExpression),
    True,
    Unary(Box<UnaryExpression>),
    VariableArguments,
}

impl builders::Expression for Expression {
    fn false_expression() -> Self { Self::False }
    fn true_expression() -> Self { Self::True }
    fn nil_expression() -> Self { Self::Nil }
    fn variable_arguments() -> Self { Self::VariableArguments }
    fn parenthese(expression: Self) -> Self { Self::Parenthese(Box::new(expression)) }
}
