use crate::nodes::Expression;

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct StringExpression {
    pub value: String,
}

impl From<String> for StringExpression {
    fn from(string: String) -> Self {
        Self { value: string }
    }
}

impl Into<Expression> for StringExpression {
    fn into(self) -> Expression {
        Expression::String(self)
    }
}