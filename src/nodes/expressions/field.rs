use crate::nodes::{
    Expression,
    Prefix,
};

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct FieldExpression {
    pub prefix: Prefix,
    pub field: String,
}

impl From<(Prefix, String)> for FieldExpression {
    fn from((prefix, field): (Prefix, String)) -> Self {
        Self {
            prefix,
            field,
        }
    }
}

impl Into<Expression> for FieldExpression {
    fn into(self) -> Expression {
        Expression::Field(Box::new(self))
    }
}
