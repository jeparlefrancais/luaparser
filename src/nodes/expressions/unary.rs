use crate::nodes::Expression;
use crate::parser::builders;

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct UnaryExpression {
    pub operator: UnaryOperator,
    pub expression: Expression,
}

impl From<(UnaryOperator, Expression)> for UnaryExpression {
    fn from((operator, expression): (UnaryOperator, Expression)) -> Self {
        Self {
            operator,
            expression,
        }
    }
}

impl Into<Expression> for UnaryExpression {
    fn into(self) -> Expression {
        Expression::Unary(Box::new(self))
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum UnaryOperator {
    Length,
    Minus,
    Not,
}

impl builders::UnaryOperator for UnaryOperator {
    fn minus() -> Self { Self::Minus }
    fn length() -> Self { Self::Length }
    fn not() -> Self { Self::Not }
}
