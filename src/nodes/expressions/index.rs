use crate::nodes::{
    Expression,
    Prefix,
};

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct IndexExpression {
    pub prefix: Prefix,
    pub index: Expression,
}

impl From<(Prefix, Expression)> for IndexExpression {
    fn from((prefix, index): (Prefix, Expression)) -> Self {
        Self {
            prefix,
            index,
        }
    }
}

impl Into<Expression> for IndexExpression {
    fn into(self) -> Expression {
        Expression::Index(Box::new(self))
    }
}
