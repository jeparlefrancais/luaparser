use crate::nodes::{
    Expression,
    FieldExpression,
    FunctionCall,
    IndexExpression,
};
use crate::parser::builders;

use core::convert::TryInto;

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Prefix {
    Call(FunctionCall),
    Field(Box<FieldExpression>),
    Identifier(String),
    Index(Box<IndexExpression>),
    Parenthese(Expression),
}

impl builders::Prefix<Expression, FunctionCall, FieldExpression, IndexExpression> for Prefix {
    fn from_name(name: String) -> Self { Self::Identifier(name) }
    fn from_parenthese(expression: Expression) -> Self { Self::Parenthese(expression) }
    fn from_call(call: FunctionCall) -> Self { Self::Call(call) }
    fn from_field(field: FieldExpression) -> Self { Self::Field(Box::new(field)) }
    fn from_index(index: IndexExpression) -> Self { Self::Index(Box::new(index)) }
}

impl Into<Expression> for Prefix {
    fn into(self) -> Expression {
        match self {
            Self::Call(call) => Expression::Call(Box::new(call)),
            Self::Field(field) => Expression::Field(field),
            Self::Identifier(name) => Expression::Identifier(name),
            Self::Index(index) => Expression::Index(index),
            Self::Parenthese(expression) => Expression::Parenthese(Box::new(expression)),
        }
    }
}

impl TryInto<FunctionCall> for Prefix {
    type Error = ();

    fn try_into(self) -> Result<FunctionCall, ()> {
        match self {
            Self::Call(call) => Ok(call),
            _ => Err(()),
        }
    }
}
