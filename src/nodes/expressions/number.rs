use crate::nodes::Expression;

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct NumberExpression {
    pub value: String,
}

impl From<String> for NumberExpression {
    fn from(value: String) -> Self {
        Self { value }
    }
}

impl Into<Expression> for NumberExpression {
    fn into(self) -> Expression {
        Expression::Number(self)
    }
}
