use crate::parser::{
    parse_expression_list,
    try_parse_table_expression,
    OptionParsingResult,
    ParsingError,
};
use crate::parser::token_utils::{
    is_symbol,
    skip_first_token,
    LuaSymbol,
};
use crate::parser::node_types::NodeTypes;
use crate::parser::node_types::builders::Arguments;

use lualexer::{Token, TokenType};

pub fn try_parse_arguments<'a, T: NodeTypes>(
    tokens: &'a [Token<'a>]
) -> OptionParsingResult<'a, T::Arguments> {
    if let Some(token) = tokens.first() {
        if is_symbol(token, LuaSymbol::OpenParenthese) {
            let (expressions, next_tokens) = parse_expression_list::<T>(skip_first_token(tokens))?;

            next_tokens.first()
                .filter(|token| is_symbol(token, LuaSymbol::CloseParenthese))
                .ok_or(ParsingError::ClosingParentheseExpected)
                .map(|_| Some((T::Arguments::from_expressions(expressions), skip_first_token(next_tokens))))

        } else if token.is_type(TokenType::String) {
            Ok(Some((T::Arguments::from_string(token.get_content().to_owned()), skip_first_token(tokens))))

        } else if let Some((table, next_tokens)) = try_parse_table_expression::<T>(tokens)? {
            Ok(Some((T::Arguments::from_table(table), next_tokens)))

        } else {
            Ok(None)
        }
    } else {
        Ok(None)
    }
}
