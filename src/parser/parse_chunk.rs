use crate::parser::{
    parse_statement,
    try_parse_last_statement,
    ParsingError,
};
use crate::parser::token_utils::{
    skip_first_token_if_is_symbol,
    LuaSymbol,
};
use crate::parser::node_types::NodeTypes;

use lualexer::Token;

pub fn parse_chunk<'a, T: NodeTypes>(tokens: &'a [Token<'a>]) -> Result<T::Block, ParsingError> {
    let mut statements = vec!();
    let mut current_tokens = tokens;

    while let Some(_token) = current_tokens.first() {
        if let Some((last_statement, mut next_tokens)) = try_parse_last_statement::<T>(current_tokens)? {
            next_tokens = skip_first_token_if_is_symbol(next_tokens, LuaSymbol::SemiColon)
                .unwrap_or(next_tokens);
            let block = T::Block::from((statements, Some(last_statement)));

            return if next_tokens.first().is_some() {
                Err(ParsingError::EndExpectedAfterLastStatement)
            } else {
                Ok(block)
            }
        }

        let (statement, next_tokens) = parse_statement::<T>(current_tokens)?;

        statements.push(statement);
        current_tokens = skip_first_token_if_is_symbol(next_tokens, LuaSymbol::SemiColon)
            .unwrap_or(next_tokens);
    };

    Ok(T::Block::from((statements, None)))
}
