use crate::parser::{
    parse_block,
    try_parse_expression,
    OptionParsingResult,
    ParsingError,
};
use crate::parser::token_utils::{
    skip_first_token_if_is_keyword,
    LuaKeyword,
};
use crate::parser::node_types::NodeTypes;

use lualexer::Token;

pub fn try_parse_while_statement<'a, T: NodeTypes>(
    tokens: &'a [Token<'a>]
) -> OptionParsingResult<'a, T::Statement> {
    if let Some(after_while_tokens) = skip_first_token_if_is_keyword(tokens, LuaKeyword::While) {
        let (condition, after_condition_tokens) = try_parse_expression::<T>(after_while_tokens)?
            .ok_or(ParsingError::ConditionExpectedForWhileStatement)?;

        let after_do_tokens = skip_first_token_if_is_keyword(after_condition_tokens, LuaKeyword::Do)
            .ok_or(ParsingError::DoKeywordExpectedForWhileStatement)?;

        let (block, next_tokens) = parse_block::<T>(after_do_tokens, LuaKeyword::End)?;

        let statement = T::WhileStatement::from((condition, block));

        Ok(Some((statement.into(), next_tokens)))
    } else {
        Ok(None)
    }
}
