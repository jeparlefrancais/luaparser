use crate::parser::{
    parse_block,
    try_parse_expression,
    OptionParsingResult,
    ParsingError,
};
use crate::parser::token_utils::{
    skip_first_token_if_is_keyword,
    LuaKeyword,
};
use crate::parser::node_types::NodeTypes;

use lualexer::Token;

pub fn try_parse_repeat_statement<'a, T: NodeTypes>(
    tokens: &'a [Token<'a>]
) -> OptionParsingResult<'a, T::Statement> {
    if let Some(after_repeat_tokens) = skip_first_token_if_is_keyword(tokens, LuaKeyword::Repeat) {
        let (block, next_tokens) = parse_block::<T>(after_repeat_tokens, LuaKeyword::Until)?;

        let (condition, after_condition_tokens) = try_parse_expression::<T>(next_tokens)?
            .ok_or(ParsingError::ConditionExpectedForRepeatStatement)?;

        let statement = T::RepeatStatement::from((condition, block));

        Ok(Some((statement.into(), after_condition_tokens)))
    } else {
        Ok(None)
    }
}
