use crate::parser::{
    parse_block,
    OptionParsingResult,
};
use crate::parser::token_utils::{
    skip_first_token_if_is_keyword,
    LuaKeyword,
};
use crate::parser::node_types::NodeTypes;

use lualexer::Token;

pub fn try_parse_do_statement<'a, T: NodeTypes>(tokens: &'a [Token<'a>]) -> OptionParsingResult<'a, T::Statement> {
    if let Some(after_do_tokens) = skip_first_token_if_is_keyword(tokens, LuaKeyword::Do) {
        let (block, next_tokens) = parse_block::<T>(after_do_tokens, LuaKeyword::End)?;

        Ok(Some((T::DoStatement::from(block).into(), next_tokens)))
    } else {
        Ok(None)
    }
}
