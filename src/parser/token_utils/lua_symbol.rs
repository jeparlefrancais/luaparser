use std::convert::TryFrom;

#[derive(Clone, Copy, Debug)]
pub enum LuaSymbol {
    Assign,
    Equal,
    NotEqual,
    LessThan,
    LessOrEqualThan,
    GreaterThan,
    GreaterOrEqualThan,
    OpenParenthese,
    CloseParenthese,
    OpenBracket,
    CloseBracket,
    OpenBrace,
    CloseBrace,
    Caret,
    Plus,
    Minus,
    Asterisk,
    Slash,
    Percent,
    Length,
    Concat,
    Comma,
    Colon,
    SemiColon,
    Dot,
    VariableArguments,
}

impl Into<&'static str> for LuaSymbol {
    fn into(self) -> &'static str {
        match self {
            Self::Assign => "=",
            Self::Equal => "==",
            Self::NotEqual => "~=",
            Self::LessThan => "<",
            Self::LessOrEqualThan => "<=",
            Self::GreaterThan => ">",
            Self::GreaterOrEqualThan => ">=",
            Self::OpenParenthese => "(",
            Self::CloseParenthese => ")",
            Self::OpenBracket => "[",
            Self::CloseBracket => "]",
            Self::OpenBrace => "{",
            Self::CloseBrace => "}",
            Self::Caret => "^",
            Self::Plus => "+",
            Self::Minus => "-",
            Self::Asterisk => "*",
            Self::Slash => "/",
            Self::Percent => "%",
            Self::Length => "#",
            Self::Concat => "..",
            Self::Comma => ",",
            Self::Colon => ":",
            Self::SemiColon => ";",
            Self::Dot => ".",
            Self::VariableArguments => "...",
        }
    }
}

impl TryFrom<&str> for LuaSymbol {
    type Error = ();

    fn try_from(string: &str) -> Result<Self, Self::Error> {
        let keyword = match string {
            "=" => Self::Assign,
            "==" => Self::Equal,
            "~=" => Self::NotEqual,
            "<" => Self::LessThan,
            "<=" => Self::LessOrEqualThan,
            ">" => Self::GreaterThan,
            ">=" => Self::GreaterOrEqualThan,
            "(" => Self::OpenParenthese,
            ")" => Self::CloseParenthese,
            "[" => Self::OpenBracket,
            "]" => Self::CloseBracket,
            "{" => Self::OpenBrace,
            "}" => Self::CloseBrace,
            "^" => Self::Caret,
            "+" => Self::Plus,
            "-" => Self::Minus,
            "*" => Self::Asterisk,
            "/" => Self::Slash,
            "%" => Self::Percent,
            "#" => Self::Length,
            ".." => Self::Concat,
            "," => Self::Comma,
            ":" => Self::Colon,
            ";" => Self::SemiColon,
            "." => Self::Dot,
            "..." => Self::VariableArguments,
            _ => return Err(()),
        };

        Ok(keyword)
    }
}
