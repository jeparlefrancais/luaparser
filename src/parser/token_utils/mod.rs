mod lua_keyword;
mod lua_symbol;

pub use lua_keyword::LuaKeyword;
pub use lua_symbol::LuaSymbol;

use lualexer::{Token, TokenType};

const EMPTY_TOKENS: &'static [Token<'static>] = &[];

pub fn skip_first_token<'a>(tokens: &'a [Token<'a>]) -> &'a [Token<'a>] {
    tokens.get(1..)
        .unwrap_or(EMPTY_TOKENS)
}

pub fn skip_tokens<'a>(tokens: &'a [Token<'a>], amount: usize) -> &'a [Token<'a>] {
    tokens.get(amount..)
        .unwrap_or(EMPTY_TOKENS)
}

#[inline]
pub fn is_keyword(token: &Token, keyword: LuaKeyword) -> bool {
    token.is_type(TokenType::Keyword)
    && token.is_slice(keyword.into())
}

#[inline]
pub fn is_one_of_keywords(token: &Token, keywords: &[LuaKeyword]) -> bool {
    token.is_type(TokenType::Keyword)
    && keywords.iter().any(|&keyword| token.is_slice(keyword.into()))
}

#[inline]
pub fn is_symbol(token: &Token, symbol: LuaSymbol) -> bool {
    token.is_type(TokenType::Symbol)
    && token.is_slice(symbol.into())
}

#[inline]
pub fn skip_first_token_if_is_keyword<'a>(
    tokens: &'a [Token<'a>],
    keyword: LuaKeyword,
) -> Option<&'a [Token<'a>]> {
    tokens.first()
        .filter(|token| is_keyword(token, keyword))
        .map(|_| skip_first_token(tokens))
}

#[inline]
pub fn skip_first_token_if_is_symbol<'a>(
    tokens: &'a [Token<'a>],
    symbol: LuaSymbol,
) -> Option<&'a [Token<'a>]> {
    tokens.first()
        .filter(|token| is_symbol(token, symbol))
        .map(|_| skip_first_token(tokens))
}
