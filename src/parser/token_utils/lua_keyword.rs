use std::convert::TryFrom;

#[derive(Clone, Copy, Debug)]
pub enum LuaKeyword {
    And,
    Break,
    Do,
    Else,
    Elseif,
    End,
    False,
    For,
    Function,
    If,
    In,
    Local,
    Nil,
    Not,
    Or,
    Repeat,
    Return,
    Then,
    True,
    Until,
    While
}

impl Into<&'static str> for LuaKeyword {
    fn into(self) -> &'static str {
        match self {
            Self::And => "and",
            Self::Break => "break",
            Self::Do => "do",
            Self::Else => "else",
            Self::Elseif => "elseif",
            Self::End => "end",
            Self::False => "false",
            Self::For => "for",
            Self::Function => "function",
            Self::If => "if",
            Self::In => "in",
            Self::Local => "local",
            Self::Nil => "nil",
            Self::Not => "not",
            Self::Or => "or",
            Self::Repeat => "repeat",
            Self::Return => "return",
            Self::Then => "then",
            Self::True => "true",
            Self::Until => "until",
            Self::While => "while",
        }
    }
}

impl TryFrom<&str> for LuaKeyword {
    type Error = ();

    fn try_from(string: &str) -> Result<Self, Self::Error> {
        let keyword = match string {
            "and" => LuaKeyword::And,
            "break" => LuaKeyword::Break,
            "do" => LuaKeyword::Do,
            "else" => LuaKeyword::Else,
            "elseif" => LuaKeyword::Elseif,
            "end" => LuaKeyword::End,
            "false" => LuaKeyword::False,
            "for" => LuaKeyword::For,
            "function" => LuaKeyword::Function,
            "if" => LuaKeyword::If,
            "in" => LuaKeyword::In,
            "local" => LuaKeyword::Local,
            "nil" => LuaKeyword::Nil,
            "not" => LuaKeyword::Not,
            "or" => LuaKeyword::Or,
            "repeat" => LuaKeyword::Repeat,
            "return" => LuaKeyword::Return,
            "then" => LuaKeyword::Then,
            "true" => LuaKeyword::True,
            "until" => LuaKeyword::Until,
            "while" => LuaKeyword::While,
            _ => return Err(()),
        };

        Ok(keyword)
    }
}
