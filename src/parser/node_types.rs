use std::convert::TryInto;

/// Collection of traits used by the parser to create or interact with the
/// abstract syntax tree nodes.
pub mod builders {
    /// A node that represents a call made to a function. In Lua, a function
    /// can be called in three different ways:
    ///  * List of expressions between parentheses (ex: `foo()`, `foo(1, 2, 3)`)
    ///  * With a string (ex: `print"Hello"`, `print'Hello'`, `print[[Hello]]`)
    ///  * With a table (ex: `foo{}`, `foo{ key = "value" }`)
    pub trait Arguments<Expression, TableExpression> {
        /// Used to build a node that represents a call from a string. The string
        /// will include the delimiters at the beginning and at the end (`"`, `'`
        /// or balanced brackets).
        fn from_string(string: String) -> Self;
        /// Used to build a node that represents a call with a table expression.
        fn from_table(table: TableExpression) -> Self;
        /// The most common way to call a function, a list of expressions between
        /// parentheses.
        fn from_expressions(expressions: Vec<Expression>) -> Self;
    }

    /// Implement this trait for the type that represents the different binary
    /// operators in Lua.
    pub trait BinaryOperator {
        /// The `and` keyword
        fn and() -> Self;
        /// The `or` keyword
        fn or() -> Self;
        /// The `==` symbol
        fn equal() -> Self;
        /// The `~=` symbol
        fn not_equal() -> Self;
        /// The `<` symbol
        fn lower_than() -> Self;
        /// The `<=` symbol
        fn lower_or_equal_than() -> Self;
        /// The `>` symbol
        fn greather_than() -> Self;
        /// The `>=` symbol
        fn greather_or_equal_than() -> Self;
        /// The `+` symbol
        fn plus() -> Self;
        /// The `-` symbol
        fn minus() -> Self;
        /// The `*` symbol
        fn asterisk() -> Self;
        /// The `/` symbol
        fn slash() -> Self;
        /// The `%` symbol
        fn percent() -> Self;
        /// The `^` symbol
        fn caret() -> Self;
        /// The `..` symbol
        fn concat() -> Self;
    }

    pub trait Expression {
        fn false_expression() -> Self;
        fn true_expression() -> Self;
        fn nil_expression() -> Self;
        fn variable_arguments() -> Self;
        fn parenthese(expression: Self) -> Self;
    }

    /// A trait to build break statements or return statements.
    pub trait LastStatement<Expression> {
        fn break_statement() -> Self;
        fn return_statement(expressions: Vec<Expression>) -> Self;
    }

    pub trait Prefix<Expression, CallExpression, FieldExpression, IndexExpression> {
        fn from_name(name: String) -> Self;
        fn from_parenthese(expression: Expression) -> Self;
        fn from_call(call: CallExpression) -> Self;
        fn from_field(field: FieldExpression) -> Self;
        fn from_index(index: IndexExpression) -> Self;
    }

    pub trait TableEntry<Expression> {
        fn from_value(value: Expression) -> Self;
        fn from_field(field: String, value: Expression) -> Self;
        fn from_index(key: Expression, value: Expression) -> Self;
    }

    /// Implement this trait for the type that represents the different unary
    /// operators in Lua.
    pub trait UnaryOperator {
        /// When a `-` is used before an expression.
        fn minus() -> Self;
        /// When a `#` is used before an expression.
        fn length() -> Self;
        /// When a `not` is used before an expression.
        fn not() -> Self;
    }

    pub trait Variable<Prefix>: Sized {
        fn try_from_prefix(prefix: Prefix) -> Result<Self, Prefix>;
    }
}

/// A trait that defines the different node types used by the parser.
pub trait NodeTypes {
    type Block: From<(Vec<Self::Statement>, Option<Self::LastStatement>)>;
    type Statement;
    type LastStatement: builders::LastStatement<Self::Expression>;

    type AssignStatement: From<(Vec<Self::Variable>, Vec<Self::Expression>)> + Into<Self::Statement>;
    type CallStatement: Into<Self::Statement>;
    type DoStatement: From<Self::Block> + Into<Self::Statement>;
    type FunctionStatement: From<(String, Vec<String>, Option<String>, Self::Block, Vec<String>, bool)> + Into<Self::Statement>;
    type GenericForStatement: From<(Vec<String>, Vec<Self::Expression>, Self::Block)> + Into<Self::Statement>;
    type IfStatement: From<(Vec<(Self::Expression, Self::Block)>, Option<Self::Block>)> + Into<Self::Statement>;
    type LocalAssignStatement: From<(Vec<String>, Vec<Self::Expression>)> + Into<Self::Statement>;
    type LocalFunctionStatement: From<(String, Vec<String>, bool, Self::Block)> + Into<Self::Statement>;
    type NumericForStatement: From<(String, Self::Expression, Self::Expression, Option<Self::Expression>, Self::Block)> + Into<Self::Statement>;
    type RepeatStatement: From<(Self::Expression, Self::Block)> + Into<Self::Statement>;
    type WhileStatement: From<(Self::Expression, Self::Block)> + Into<Self::Statement>;

    type Arguments: builders::Arguments<Self::Expression, Self::TableExpression>;
    type Prefix: builders::Prefix<Self::Expression, Self::CallExpression, Self::FieldExpression, Self::IndexExpression>
        + Into<Self::Expression> + TryInto<Self::CallStatement>;
    type Variable: builders::Variable<Self::Prefix>;

    type Expression: builders::Expression;

    type BinaryOperator: Eq + builders::BinaryOperator;
    type BinaryExpression: From<(Self::Expression, Self::BinaryOperator, Self::Expression)> + Into<Self::Expression>;
    type CallExpression: From<(Self::Prefix, Self::Arguments, Option<String>)> + Into<Self::Expression>;
    type FieldExpression: From<(Self::Prefix, String)> + Into<Self::Expression>;
    type FunctionExpression: From<(Vec<String>, bool, Self::Block)> + Into<Self::Expression>;
    type IndexExpression: From<(Self::Prefix, Self::Expression)> + Into<Self::Expression>;
    type NumberExpression: From<String> + Into<Self::Expression>;
    type StringExpression: From<String> + Into<Self::Expression>;
    type TableEntry: builders::TableEntry<Self::Expression>;
    type TableExpression: From<Vec<Self::TableEntry>> + Into<Self::Expression>;
    type UnaryOperator: builders::UnaryOperator;
    type UnaryExpression: From<(Self::UnaryOperator, Self::Expression)> + Into<Self::Expression>;
}
