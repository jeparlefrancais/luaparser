use crate::parser::{
    try_parse_single_expression,
    parse_right_expression,
    ParsingResult,
    ParsingError,
};
use crate::parser::node_types::NodeTypes;

use lualexer::Token;

const UNARY_OPERATOR_PRECEDENCE: usize = 7;

pub fn parse_unary_expression<'a, T: NodeTypes>(
    operator: T::UnaryOperator,
    tokens: &'a [Token<'a>]
) -> ParsingResult<'a, T::UnaryExpression> {
    let (first_expression, next_tokens) = try_parse_single_expression::<T>(tokens)?
        .ok_or(ParsingError::ExpressionExpectedAfterUnaryOperator)?;

    let (expression, next_tokens) = parse_right_expression::<T>(
        first_expression,
        next_tokens,
        UNARY_OPERATOR_PRECEDENCE
    )?;

    Ok((T::UnaryExpression::from((operator, expression)), next_tokens))
}
