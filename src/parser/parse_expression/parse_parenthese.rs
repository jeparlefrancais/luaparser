use crate::parser::{
    try_parse_expression,
    OptionParsingResult,
    ParsingError,
};
use crate::parser::token_utils::{
    is_symbol,
    skip_first_token,
    LuaSymbol,
};
use crate::parser::node_types::NodeTypes;

use lualexer::Token;

pub fn try_parse_parenthese_expression<'a, T: NodeTypes>(
    tokens: &'a [Token<'a>],
) -> OptionParsingResult<'a, T::Expression> {
    if let Some(_) = tokens.first()
        .filter(|token| is_symbol(token, LuaSymbol::OpenParenthese))
    {
        let (sub_expression, next_tokens) = try_parse_expression::<T>(skip_first_token(tokens))?
            .ok_or(ParsingError::ExpressionExpectedAfterOpeningParenthese)?;

        next_tokens.first()
            .filter(|token| is_symbol(token, LuaSymbol::CloseParenthese))
            .map(|_| Some((sub_expression, skip_first_token(next_tokens))))
            .ok_or(ParsingError::ClosingParentheseExpected)

    } else {
        Ok(None)
    }
}
