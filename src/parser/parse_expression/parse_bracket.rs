use crate::parser::{
    try_parse_expression,
    OptionParsingResult,
    ParsingError,
};
use crate::parser::token_utils::{
    is_symbol,
    skip_first_token,
    LuaSymbol,
};
use crate::parser::node_types::NodeTypes;

use lualexer::Token;

pub fn try_parse_bracket<'a, T: NodeTypes>(
    tokens: &'a [Token<'a>]
) -> OptionParsingResult<'a, T::Expression> {
    if let Some(_) = tokens.first()
        .filter(|token| is_symbol(token, LuaSymbol::OpenBracket))
    {
        if let Some((expression, next_tokens)) = try_parse_expression::<T>(skip_first_token(tokens))? {
            next_tokens.first()
                .filter(|token| is_symbol(token, LuaSymbol::CloseBracket))
                .map(|_| Some((expression, skip_first_token(next_tokens))))
                .ok_or(ParsingError::ClosingBracketExpected)
        } else {
            Ok(None)
        }
    } else {
        Ok(None)
    }
}
