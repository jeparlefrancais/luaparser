use crate::parser::{
    try_parse_prefix_expression,
    try_parse_primary_expression,
    OptionParsingResult,
};
use crate::parser::node_types::NodeTypes;

use lualexer::Token;

pub fn try_parse_single_expression<'a, T: NodeTypes>(
    tokens: &'a [Token<'a>]
) -> OptionParsingResult<'a, T::Expression> {
    if let Some((primary_expression, next_tokens)) = try_parse_primary_expression::<T>(tokens)? {
        Ok(Some((primary_expression, next_tokens)))
    } else if let Some((prefix, next_tokens)) = try_parse_prefix_expression::<T>(tokens)? {
        Ok(Some((prefix.into(), next_tokens)))
    } else {
        Ok(None)
    }
}
