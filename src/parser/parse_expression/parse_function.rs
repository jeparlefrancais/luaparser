use crate::parser::{
    parse_function_body,
    ParsingResult,
    ParsingError,
};
use crate::parser::token_utils::{
    skip_first_token_if_is_keyword,
    LuaKeyword,
};
use crate::parser::node_types::NodeTypes;

use lualexer::Token;

pub fn parse_function_expression<'a, T: NodeTypes>(
    tokens: &'a [Token<'a>],
) -> ParsingResult<'a, T::FunctionExpression> {
    let after_function_tokens = skip_first_token_if_is_keyword(tokens, LuaKeyword::Function)
        .ok_or(ParsingError::FunctionKeywordExpected)?;

    let (values, next_tokens) = parse_function_body::<T>(after_function_tokens)?;
    let (parameters, is_variadic, block) = values;

    Ok((T::FunctionExpression::from((parameters, is_variadic, block)), next_tokens))
}
