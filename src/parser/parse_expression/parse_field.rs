use crate::parser::token_utils::{
    skip_tokens,
    skip_first_token_if_is_symbol,
    LuaSymbol,
};

use lualexer::{Token, TokenType};

pub fn try_parse_field<'a>(tokens: &'a [Token<'a>]) -> Option<(String, &'a [Token<'a>])> {
    skip_first_token_if_is_symbol(tokens, LuaSymbol::Dot)
        .and_then(|tokens| tokens.first())
        .filter(|token| token.is_type(TokenType::Identifier))
        .map(|token| (token.get_content().to_owned(), skip_tokens(tokens, 2)))
}
