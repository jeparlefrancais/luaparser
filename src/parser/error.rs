use lualexer::{Token, LexerErrorType};

/// This enum represents the various errors that may happen while parsing tokens
/// into an AST (abstract syntax tree).
#[derive(Debug, Clone)]
pub enum ParsingError {
    /// After parsing the last statement of a block (break or return statement), the parser
    /// expects a token to close the block (e.g. `end`, `until`, `else`).
    EndOfBlockExpectedAfterLastStatement,
    /// After parsing the last statement of a chunk (an entire file for example), the parser
    /// expects no more tokens.
    EndExpectedAfterLastStatement,
    /// After parsing a binary operator (`+`, `and`, `or`, `==`, ...), the parser expects an
    /// expression to form a binary expression.
    RightExpressionExpected,
    /// The parser was expecting a token that can start a statement.
    UnexpectedTokenForStatement,
    /// When parsing a block, a specific token is expected to close that block. For example,
    /// when parsing a do statement, the parser expects the keyword `end` to close the inner
    /// block. For a repeat statement, the parser expects the `until` keyword.
    TokenExpectedToCloseBlock,
    /// The parser was expecting the keyword `function`
    FunctionKeywordExpected,
    /// The parser was expecting a `(` to begin the parameters definition of a function.
    OpeningParentheseExpectedForFunction,
    /// The parser was expecting a `)` to close the parameters definition of a function.
    ClosingParentheseExpectedForFunction,
    /// The parser expects a list of identifiers that may end with the variable arguments symbol
    /// (`...`), separated by commas. The error is return if another token is found.
    InvalidFunctionParameter,
    /// The parser was expecting an expression after a unary operator (`#`, `-` or `not`)
    ExpressionExpectedAfterUnaryOperator,
    /// After opening a parenthese (as an expression, not a call to a function or
    /// method), the parser expects an expression.
    ExpressionExpectedAfterOpeningParenthese,
    /// When parsing a parenthese expression, the parser expects a closing parenthese after the
    /// expression.
    ClosingParentheseExpected,
    /// When opening a bracket (`[`) to index another value, the parse expects an expression
    /// followed with a closing bracket (`]`).
    ClosingBracketExpected,
    /// When parsing a method call, the parser expects arguments after the method name.
    ArgumentsExpectedAfterMethod,
    /// When parsing a table, the parser expects a closing brace to close that table.
    ClosingBraceExpected,
    /// The parser expects an expression after the `=` when parsing a field entry in a table
    /// (`{ key = expect_expression_here }`).
    ExpressionExpectedForTableEntry,
    /// The parser expects an expression between brackets when parsing a table entry key
    /// (`{ [expect_expression_here] = value }`).
    ExpressionExpectedForTableKey,
    /// After parsing a table entry key (`[key] = ...`), the parser expects an assign symbol (`=`).
    AssignSymbolExpectedAfterTableKey,
    /// When parsing a table key using brackets, the parser expects a closing bracket after the
    /// expression.
    ClosingBracketExpectedForTableKey,
    /// When parsing a while statement, the parser expects an expression after the while keyword.
    ConditionExpectedForWhileStatement,
    /// When parsing a while statement, the parser expects the keyword `do` after the condition.
    DoKeywordExpectedForWhileStatement,
    /// When parsing a repeat statement, the parser expects an expression after the until keyword.
    ConditionExpectedForRepeatStatement,
    /// When parsing an if statement, an expression is expected after the `if` keyword.
    ConditionExpectedForIfStatement,
    /// When parsing an if statement, an expression is expected after an `elseif` keyword.
    ConditionExpectedForElseIfClause,
    /// When parsing an if statement, a `then` keyword is expected after the condition.
    ThenKeywordExpectedForIfStatement,
    /// When parsing an else-if clase, a `then` keyword is expected after the condition.
    ThenKeywordExpectedForElseifClause,
    /// When declaring a numeric for statement, the parser expects an expression after the assign
    /// symbol (`=`) to specify the starting value.
    StartExpressionExpectedForNumericFor,
    /// When declaring a numeric for statement, the parser expects an expression after the comma
    /// (`,`) to specify the ending value.
    EndExpressionExpectedForNumericFor,
    /// When declaring a numeric for statement, the parser expects an expression after the comma
    /// following the end expression.
    StepExpressionExpectedForNumericFor,
    /// When declaring a numeric for statement, the parser expects a comma after the start
    /// expression.
    CommaExpectedAfterStartExpressionOfNumericFor,
    /// When declaring a numeric for statement, the parser expects the keyword `do` after the
    /// ending expression or the step expression.
    DoExpectedForNumericFor,
    /// An identifier is expected after a `for` keyword.
    IdentifierExpectedAfterForKeyword,
    /// When parsing the identifier list of a generic for statement, the parser expects an
    /// identifier after each comma.
    IdentifierExpectedForGenericFor,
    /// After parsing the identifier list of a generic for statement, the parser expects the `in`
    /// keyword.
    InKeywordExpectedForGenericFor,
    /// After the `in` keyword, an expression is expected.
    ExpressionExpectedForGenericFor,
    /// After parsing the expression list of a generic for statement, the parser
    /// expects the `do` keyword.
    DoKeywordExpectedForGenericFor,
    /// When parsing a local assignement, the parser expects at least one identifier.
    IdentifierExpectedForLocalAssign,
    /// When parsing a function definition, an identifier is expected after the `function` keyword
    ///  or after every dot (`.`) symbol in the function name.
    IdentifierExpectedForFunction,
    /// The parser expects an identifier to access a field of an expression.
    AssignSymbolExpectedForAssignStatement,
    /// When parsing an assign statement, the parser expects an expression after the assign (`=`)
    /// symbol and after each commas following an expression after the assign symbol.
    ExpressionExpectedForAssignStatement,
    /// After a comma following a variable in an assign statement, another variable is expected.
    VariableExpectedForAssignStatement,
    /// Wraps the error thrown by the tokenization process.
    Lexer(LexerErrorType),
}

pub type ParsingResult<'a, T> = Result<(T, &'a [Token<'a>]), ParsingError>;

pub type OptionParsingResult<'a, T> = Result<Option<(T, &'a [Token<'a>])>, ParsingError>;
