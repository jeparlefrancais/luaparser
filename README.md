[![pipeline status](https://gitlab.com/jeparlefrancais/luaparser/badges/master/pipeline.svg)](https://gitlab.com/jeparlefrancais/luaparser/commits/master)

# luaparser

A parser that reads Lua 5.1 code and produces an abstract syntax tree (AST).

The parser itself does not know the concrete type of the nodes in the AST. It is templated so you can provide your own type of nodes. If you don't want to provide your own nodes, the crate comes with a basic implementation of each nodes that you can use. If you want to disable the built-in nodes, turn off the default features in your [Cargo.toml](https://doc.rust-lang.org/cargo/reference/specifying-dependencies.html#choosing-features).

This project is in early development so things could change. I would not recommend it for any serious project yet.

My goal with this project was to write a generic Lua parser that abstract the type of the produced abstract syntax tree. Technically, it kind of does but I am not sure yetif it is really useful in practice. The reason behind that idea is that I wanted to create a parser that I could use for an obfuscation tool (so it's goal is mostly just to be fast) but that I could also use for a formatting/style checking tool (this requires a parser that keeps all information about whitespaces).

# License

luaparser is available under the MIT license. See [LICENSE.txt](LICENSE.txt) for details.
